package com.androidprojects.aazz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        int arr[] = {3,5,1,2,4, 10, 4, 11};
        quicksort(arr, 0, arr.length - 1);
        Log.d(TAG, "onCreate: " + Arrays.toString(arr));
    }

    private void quicksort(int[] arr, int l, int r) {
        if (r - l <= 0) {
            return;
        } else {
            int pivot = arr[r];
            int part = partition(l, r, pivot, arr);
            quicksort(arr, l, part - 1);
            quicksort(arr, part + 1, r);
        }
    }

    private int partition(int left, int right, int piv, int[] arr) {
        int leftPtr = left - 1;
        int rightPtr = right;
        while (true) {
            while (arr[++leftPtr] < piv);
            while (rightPtr > 0 && arr[--rightPtr] > piv);

            if (leftPtr >= rightPtr) {
                break;
            } else {
                swap(arr, leftPtr, rightPtr);
            }
        }
        swap(arr, leftPtr, right);
        return leftPtr;
    }

    private void swap(int[] arr, int leftPtr, int rightPtr) {
        int temp = arr[leftPtr];
        arr[leftPtr] = arr[rightPtr];
        arr[rightPtr] = temp;
    }
}
